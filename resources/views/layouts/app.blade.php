<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Задание') }} @yield('title')</title>

    <!-- Styles -->
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    @yield('link')
    @yield('style')
    <script src="{{ asset('js/bootstrap.js') }}"></script>
	<script src="{{ asset('js/jquery.js') }}"></script>
    @yield('script')
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
</body>
</html>
