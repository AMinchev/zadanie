@extends('layouts.app')

@section('link')
    <link href="{{ asset('css/theme.default.css') }}" rel="stylesheet">
@endsection

@section('script')
    <script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
@endsection

@section('content')
	<div class="row">
        <div class="col">
			<table class="table table-bordered table-striped table-hover">
				<thead><tr>
				@foreach ($titles as $title)
					<th>{{ $title }}</th>
				@endforeach
				</tr></thead><tbody>
			@foreach ($data as $row)
				<tr onclick="alert('Бих редактирал реда с ID={{ $row->id ?? $row->ID }}')">
				@foreach ($row as $cell)
					<td>{{ $cell }}</td>
				@endforeach
				</tr>
			@endforeach
			</tbody></table>
		</div>
	</div>

<script type="text/javascript" charset="utf-8">
$('.table').tablesorter();
</script>
@endsection