<?php

namespace App\Http\Controllers;

use DB, File;
use Illuminate\Http\Request;
use PHPSQLParser\PHPSQLParser;

class IniController extends Controller
{
    public function index($queryRow = null)
    {
		//parse file
		$path = database_path('/sql/data.ini');
		$lines = preg_split('/\r\n|\n|\r/', trim((File::get($path))));

		foreach ($lines as $line) {
			[$key, $value] = explode('=', $line, 2);

			if ($key && $value) {
				$parsed[$key] = $value;
			}
		}

		//get data
		$data = DB::select($queryRow && isset($parsed[$queryRow]) ? $parsed[$queryRow] : $parsed['QueryStr']);

		//get names of columns from db data
		foreach ($data[0] as $key => $value) {
			$columns["_{$key}"] = $key;
		}

		//replace titles from select with that of config if needed
		$titles = array_merge($columns, $parsed);

		//hide columns
		foreach ($titles as $key => $value) {
			if ($key{0} != '_') {
				if (($value == 1) && (substr($key, 0, 4) == 'hide') && ($name = substr($key, 5)) && (strtoupper($name) != 'ID') && isset($titles["_{$name}"])) {
					unset($titles["_{$name}"]);

					foreach ($data as $row) {
						unset($row->$name);
					}
				}

				unset($titles[$key]);
			}
		}

		//dd($parsed, $columns, $titles, $data);
        return view('ini.index', compact('titles', 'data'));
    }
}
