CREATE TABLE `n_Countries` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `Country_BG` varchar(50) COLLATE cp1251_bulgarian_ci NOT NULL DEFAULT '',
  `ShortName_BG` varchar(5) COLLATE cp1251_bulgarian_ci NOT NULL DEFAULT '',
  `Country_EN` varchar(50) COLLATE cp1251_bulgarian_ci NOT NULL DEFAULT '',
  `ShortName_EN` varchar(5) COLLATE cp1251_bulgarian_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Country_BG` (`Country_BG`),
  UNIQUE KEY `Country_EN` (`Country_EN`),
  UNIQUE KEY `ShortName_BG` (`ShortName_BG`),
  UNIQUE KEY `ShortName_EN` (`ShortName_EN`)
) ENGINE=InnoDB 

INSERT INTO `n_Countries` VALUES (1,'България','БГ','Bulgaria','BG'),(2,'Австрия','АВ-Я','Austria','AT'),(3,'Албания','АЛБ','Albania','AL'),(4,'Андора','АНД','Andorra','AD'),(5,'Армения','АРМ','Armenia','ARM'),(6,'Азейрбайджан','АЗЕР','Azerbaijan','AZ'),(7,'Беларус','БЛРС','Belarus','BLR'),(8,'Белгия','БЕЛ','Belgium','BEL'),(9,'Босна и Херцеговина','БиХ','Bosnia and Herzegovina','BH'),(11,'Ватикан','ВТКН','Vatican','VA'),(12,'Германия','ФРГ','Germany','G'),(13,'Грузия','ГРУЗ','Georgia','GEO'),(14,'Гърция','ГРЦ','Greece','GR'),(15,'Дания','ДАН','Denmark','DK'),(16,'Естония','ЕСТ','Estonia','EST'),(17,'Ирландия','ИРЛ','Ireland','IRL'),(18,'Исландия','ИСЛ','Iceland','ISL'),(19,'Испания','ИСП','Spain','ESP'),(20,'Италия','ИТ','Italy','IT'),(21,'Казахстан','КАЗАХ','Kazakhstan','KZ'),(22,'Кипър','КИПЪР','Cyprus','CY'),(23,'Латвия','ЛАТВ','Latvia','LVA'),(24,'Литва','ЛИТВА','Lithuania','LT'),(25,'Лихтенщайн','ЛИХТ','Liechtenstein','LI'),(26,'Люксембург','ЛЮКС','Luxembourg','LUX'),(27,'Македония','МК','Macedonia','MK'),(28,'Малта','МАЛТА','Malta','MLT'),(29,'Молдова','МЛДВ','Moldova','MDA'),(30,'Монако','МНК','Monaco','MCO'),(31,'Норвегия','НРВГ','Norway','NOR'),(32,'Великобритания','Англи','United Kingdom','UK'),(33,'Полша','ПОЛ','Poland','PL'),(34,'Португалия','ПОРТ','Portugal','PRT'),(35,'Румъния','РУМ','Romania','RO'),(36,'Русия','РУС','Russia','RUS'),(37,'Сан Марино','САНМ','San Marino','SMR'),(38,'Словакия','СЛВК','Slovakia','SVK'),(39,'Словения','СЛВН','Slovenia','SVN'),(40,'Сърбия','СРБ','Serbia','SRB'),(41,'Турция','ТУР','Turkey','TR'),(42,'Украйна','УКР','Ukraine','UKR'),(43,'Унгария','УНГ','Hungary','HU'),(44,'Финландия','ФИН','Finland','FI'),(45,'Франция','ФР','France','FR'),(46,'Холандия','ХЛНД','Netherlands','NL'),(47,'Хърватия','ХРВ','Croatia','HR'),(48,'Черна гора','ЧГР','Montenegro','MNG'),(49,'Чехия','ЧЕХ','Czech Republic','CZ'),(50,'Швейцария','ШВЦР','Switzerland','SWZ'),(51,'Швеция','ШВЦ','Sweden','SWE'),(52,'Китай','Китай','China','PRC'),(53,'Хонг Конг','HK','Hong Kong','HK'),(54,'Съединени Американски щати','САЩ','United States of America','USA');
